package com.e.taller3m.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.e.taller3m.model.Product;

import java.util.List;

@Dao
public interface ProductoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertProductos(Product... products);

    @Query("SELECT * FROM product")
    List<Product> getAll();

    @Query("SELECT * FROM product p WHERE p.code = :codigo LIMIT 1") // me va a devolver un solo producto
    Product getByCodigo(int codigo);


    @Query("SELECT * FROM product p WHERE p.category=:category")
    List<Product> getProductsByCategory(String category);

    @Update
    void updateStockByProductCode(Product product);
}