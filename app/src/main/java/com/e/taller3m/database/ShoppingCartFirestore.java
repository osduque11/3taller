package com.e.taller3m.database;

import android.content.Context;

import androidx.annotation.Nullable;

import com.e.taller3m.model.ShoppingCart;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartFirestore implements ShoppingCarFirestore {

    public static final String TAG = ShoppingCartFirestore.class.getSimpleName();

    public static final String SHOPPING_CART_REFERENCE = "ShoppingCart";

    private ShoppingCartDao shoppingCartDao;
    private FirebaseFirestore mInstance;

    public ShoppingCartFirestore(Context context) {
        mInstance = FirebaseFirestore.getInstance();
    }

    @Override
    public void insertProductos(ShoppingCart... cartProducts) {
        CollectionReference productsReference = mInstance.collection(SHOPPING_CART_REFERENCE);
        for (ShoppingCart shopping : cartProducts) {
            productsReference.document(String.valueOf(shopping.getCode())).set(shopping);
        }
    }

    @Override
    public Task<QuerySnapshot> getAll() {
        final List<ShoppingCart> shoppingCarts = new ArrayList<>();
        CollectionReference shoppingReference = mInstance.collection(SHOPPING_CART_REFERENCE);

        return shoppingReference.get();
    }


    public ShoppingCart getShoppingCartByCode(int code) {
        final ShoppingCart[] p = {null};

        DocumentReference shoppingCartRef = mInstance.collection(SHOPPING_CART_REFERENCE)
                .document(String.valueOf(code));

        shoppingCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    p[0] = documentSnapshot.toObject(ShoppingCart.class);
                }

            }
        });


        return p[0];
    }

    @Override
    public void deleteProduct(ShoppingCart product) {
        CollectionReference shoppingReference = mInstance.collection(SHOPPING_CART_REFERENCE);
        shoppingReference.document(String.valueOf(product.getCode())).delete();
    }

}
