package com.e.taller3m.database;

import androidx.room.Dao;

import com.e.taller3m.model.Product;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ProductoFirestore {
    public void insertProductos(ArrayList<Product> products);
    List<Product> getAll();
    Task<DocumentSnapshot> getByCodigo(int codigo);
    Query getProductsByCategory(String category);
    void updateStockByProductCode(Product product);
}