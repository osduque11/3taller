package com.e.taller3m.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.e.taller3m.model.Product;
import com.e.taller3m.model.ShoppingCart;

@Database(entities = {Product.class, ShoppingCart.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DB_NAME = "bestDeal";
    public abstract ProductoDao productoDao();
    public abstract ShoppingCartDao shoppingCartDao();
}