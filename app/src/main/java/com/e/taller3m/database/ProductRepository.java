package com.e.taller3m.database;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Room;

import com.e.taller3m.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProductRepository implements ProductoDao {

    private ProductoDao productoDao;
    Context mContext;

    public ProductRepository(Context context) {
        AppDatabase db = Room.databaseBuilder(context, AppDatabase.class, AppDatabase.DB_NAME).build();
        productoDao = db.productoDao();
        mContext = context;
    }

    public void insertProductos(Product... products) {
        AsyncTask<Product, Void, Void> guardarProductoAsync = new AsyncTask<Product, Void, Void>() {
            @Override
            protected Void doInBackground(Product... products) {
                productoDao.insertProductos(products);
                return null;
            }
        };
        guardarProductoAsync.execute(products);
    }

    public List<Product> getAll() {
        List<Product> productos = new ArrayList<>();
        try {
            productos = new AsyncTask<Void, Void, List<Product>>() {
                @Override
                protected List<Product> doInBackground(Void... voids) {
                    return productoDao.getAll();
                }

            }.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return productos;
    }

    public Product getByCodigo(int codigo) {
        Product p = new Product();

        try {
            AsyncTask<Integer, Void, Product> getProductoByCodigoAsync = new AsyncTask<Integer, Void, Product>() {
                @Override
                protected Product doInBackground(Integer... integers) {
                    return productoDao.getByCodigo(integers[0]);
                }
            };

            p = getProductoByCodigoAsync.execute(codigo).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return p;
    }


    public List<Product> getProductsByCategory(String category) {
        List<Product> productos = new ArrayList<>();
        try {
            AsyncTask<String, Void, ArrayList<Product>> getProductsByCategory = new AsyncTask<String, Void, ArrayList<Product>>() {

                @Override
                protected ArrayList<Product> doInBackground(String... strings) {
                    return (ArrayList<Product>) productoDao.getProductsByCategory(strings[0]);
                }
            };

            productos = getProductsByCategory.execute(category).get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return productos;
    }


    public void updateStockByProductCode(final Product product) {
        AsyncTask<Product, Void, Void> updateProduct = new AsyncTask<Product, Void, Void>() {
            @Override
            protected Void doInBackground(Product... products) {
                productoDao.updateStockByProductCode(products[0]);
                return null;
            }
        };
        updateProduct.execute(product);
    }

}
