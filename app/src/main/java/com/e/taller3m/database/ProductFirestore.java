package com.e.taller3m.database;

import android.content.Context;

import androidx.annotation.Nullable;

import com.e.taller3m.model.Product;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ProductFirestore implements ProductoFirestore {

    private ProductoDao productoDao;
    private FirebaseFirestore mInstance;
    FirebaseApp db;
    Context mContext;

    public ProductFirestore(Context context) {
        mContext = context;
        mInstance = FirebaseFirestore.getInstance();
    }

    public void insertProductos(ArrayList<Product> products) {
        CollectionReference productsReference = mInstance.collection("Products");
        for(Product product: products){
            productsReference.document(String.valueOf(product.getCode())).set(product);
        }
    }

    public List<Product> getAll() {
        final List<Product> productos = new ArrayList<>();
        mInstance.collection("Products").orderBy("code").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for(DocumentChange change : queryDocumentSnapshots.getDocumentChanges()){
                    DocumentSnapshot snapshot = change.getDocument();
                    Product product = snapshot.toObject(Product.class);
                    productos.add(product);
                }
            }
        });
        return productos;
    }

    public Task<DocumentSnapshot> getByCodigo(int codigo) {
        final Product[] p = {null};

        DocumentReference productRef = mInstance.collection("Products")
                .document(String.valueOf(codigo));

        return productRef.get();

    }


    public Query getProductsByCategory(String category) {
        final List<Product> productos = new ArrayList<>();
        CollectionReference productRef = mInstance.collection("Products");

        return productRef.whereEqualTo("category",category);

    }


    public void updateStockByProductCode(final Product product) {

        DocumentReference productRef = mInstance.collection("Products")
                .document(String.valueOf(product.getCode()));

        productRef.set(product);
    }

}
