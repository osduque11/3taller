package com.e.taller3m.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Room;

import com.e.taller3m.model.Product;
import com.e.taller3m.model.ShoppingCart;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CartShoppingRepository implements ShoppingCartDao {

    public static final String TAG = CartShoppingRepository.class.getSimpleName();

    private final AppDatabase db;
    private ShoppingCartDao shoppingCartDao;

    public CartShoppingRepository(Context context) {
        db = Room.databaseBuilder(context, AppDatabase.class, AppDatabase.DB_NAME).build();
        shoppingCartDao = db.shoppingCartDao();
    }

    @Override
    public void insertProductos(ShoppingCart... cartProducts) {
        AsyncTask<ShoppingCart, Void, Void> guardarShoppingCart = new AsyncTask<ShoppingCart, Void, Void>() {
            @Override
            protected Void doInBackground(ShoppingCart... products) {
                ShoppingCart tempProduct = products[0];
                shoppingCartDao.insertProductos(tempProduct);
                return null;
            }
        };
        guardarShoppingCart.execute(cartProducts);
    }

    @Override
    public List<ShoppingCart> getAll() {
        List<ShoppingCart> productos = new ArrayList<>();
        try {
            productos = new AsyncTask<Void, Void, List<ShoppingCart>>() {
                @Override
                protected List<ShoppingCart> doInBackground(Void... voids) {
                    return shoppingCartDao.getAll();
                }

            }.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return productos;

    }


    public ShoppingCart getShoppingCartByCode(int code) {
        ShoppingCart p = null;

        try {
            AsyncTask<Integer, Void, ShoppingCart> getProductoByCodigoAsync = new AsyncTask<Integer, Void, ShoppingCart>() {
                @Override
                protected ShoppingCart doInBackground(Integer... integers) {
                    return shoppingCartDao.getShoppingCartByCode(integers[0]);
                }
            };

            p = getProductoByCodigoAsync.execute(code).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return p;
    }

    @Override
    public void deleteProduct(ShoppingCart product) {
        AsyncTask<ShoppingCart, Void, Void> deleteShoppingCart = new AsyncTask<ShoppingCart, Void, Void>() {
            @Override
            protected Void doInBackground(ShoppingCart... products) {
                ShoppingCart tempProduct = products[0];
                Log.i(TAG, "doInBackground: Se va a Borrar el producto =>"+tempProduct.toString());
                shoppingCartDao.deleteProduct(tempProduct);
                return null;
            }
        };
        deleteShoppingCart.execute(product);

    }

}
