package com.e.taller3m.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.e.taller3m.model.Product;
import com.e.taller3m.model.ShoppingCart;

import java.util.List;

@Dao
public interface ShoppingCartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertProductos(ShoppingCart... cartProducts);

    @Query("SELECT * FROM ShoppingCart")
    List<ShoppingCart> getAll();

    @Query("SELECT * FROM ShoppingCart sc WHERE sc.code =:code LIMIT 1")
    ShoppingCart getShoppingCartByCode(int code);

    @Delete
    void deleteProduct(ShoppingCart product);

}