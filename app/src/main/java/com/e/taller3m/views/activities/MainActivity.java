package com.e.taller3m.views.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.e.taller3m.R;
import com.e.taller3m.database.AppDatabase;
import com.e.taller3m.database.ProductFirestore;
import com.e.taller3m.database.ProductRepository;
import com.e.taller3m.model.Product;
import com.e.taller3m.views.fragments.CartFragment;
import com.e.taller3m.views.fragments.CategoriesFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.core.FirestoreClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    CategoriesFragment categoriesFragment;
    CartFragment cartFragment;
    AppDatabase db;
    ArrayList<Product> listProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navView.setSelectedItemId(R.id.navigation_categories);

        db = Room.databaseBuilder(this, AppDatabase.class, "bestDeal").build();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Toast.makeText(this, "Name: "+db.getApp().getName(), Toast.LENGTH_SHORT).show();

        SharedPreferences sesionPref = this.getSharedPreferences("sesionPref", Context.MODE_PRIVATE);

        if (!sesionPref.getBoolean("SesionIniciada", false)) {
            SharedPreferences.Editor editPref = sesionPref.edit();
            editPref.putBoolean("SesionIniciada", true);
            editPref.apply();
            setData();
        }
    }

    private void setData() {
        listProduct = new ArrayList<>();
        listProduct.add(new Product(3459499,
                "Huawei P20 128GB",
                "El HUAWEI P20 abre un nuevo camino en cuanto a la fotografía de los smartphones presentando " +
                        "la nueva cámara dual Leica.",
                "Smartphone", "1399900",
                12, R.drawable.simage1));
        listProduct.add(new Product(3493329, "Moto G6",
                "Con un diseño único y sofisticado, moto G6 tiene un acabado trasero de vidrio 3D con un " +
                        "efecto visual innovador que lo hace aún más elegante.", "Smartphone", "399900", 2, R.drawable.simage2));
        listProduct.add(new Product(3929422, "iPhone 7 Plus 32GB",
                "El Apple iPhone 7 Plus mejora las características del iPhone 6S Plus con una cámara dual de " +
                        "12 MP que permite realizar diferentes efectos de profundidad de campo.",
                "Smartphone", "2499900", 8, R.drawable.simage3));
        listProduct.add(new Product(4037391, "Samsung Galaxy S10 128GB",
                "Cinco lentes muchas posibilidades para capturar tus mejores y más bellos instantes.",
                "Smartphone", "3299900", 1, R.drawable.simage4));
        listProduct.add(new Product(3539930, "Tenis Moda Mujer Tubular Shadow CK",
                "No tiene", "Tenis", "214990", 9, R.drawable.timage2));
        listProduct.add(new Product(3789087, "Tenis Moda Mujer Viale",
                "No tiene", "Tenis", "149990", 7, R.drawable.timage1));
        listProduct.add(new Product(3926348, "Tenis Moda Hombre Air Huarache Run PRM Zip",
                "No tiene", "Tenis", "487990", 5, R.drawable.timage3));
        listProduct.add(new Product(4074953, "Tenis Moda Hombre Storm Pulse",
                "No tiene", "Tenis", "389900", 2, R.drawable.timage4));
        listProduct.add(new Product(3707572, "Mochila",
                "Vélez", "Morrales", "159990", 0, R.drawable.mimage4));
        listProduct.add(new Product(3737202, "Morral Colegio",
                "CAT", "Morrales", "229900", 1, R.drawable.mimage1));
        listProduct.add(new Product(3885477, "Florel",
                "AMPHORA", "Morrales", "69990", 4, R.drawable.mimage2));
        listProduct.add(new Product(3885478, "Mochila Flore Denim",
                "AMPHORA", "Morrales", "69990", 3, R.drawable.mimage3));
        listProduct.add(new Product(2775954, "Marvel Heroes se RReúne\n",
                "Juguetes didácticos. Marvel Heroes se RReúne con Cabezas de Latas.",
                "Juguetes", "59990", 6, R.drawable.jimage1));
        listProduct.add(new Product(880998098, "Rodadero y Columpio",
                "Juego de exterior diseñado tanto para niños como para niñas.",
                "Juguetes", "349990", 8, R.drawable.jimage2));
        listProduct.add(new Product(881323287, "Hoverboard",
                "Plataforma con dos ruedas y dos espacios para poner los pies, que funciona con una batería" +
                        " recargable de litio. Gracias a ella, esta máquina puede desplazarse a una velocidad superior " +
                        "a los 10 kilómetros por hora.", "Juguetes",
                "559990", 8, R.drawable.jimage3));
        listProduct.add(new Product(881358467, "Foldable Drone 2.0MP Wifi HD Gran Angular",
                "Con la función wifi se puede conectar la aplicación, sistema APK para tomar fotos, video, " +
                        "transmisión en tiempo real a través de la imagen de la cámara del teléfono.",
                "Juguetes", "249990", 9, R.drawable.jimage4));
        listProduct.add(new Product(1269014, "Hp Pavilion 15-Cx0001la Corei5 8gb 1tb Gtx4gb 15-6",
                "La nueva HP Pavilion Gaming cuenta con la última tecnología en gráficos NVIDIA® GeForce® GTX " +
                        "y procesador Intel® para juegos, creación de contenidos y aplicaciones de alta productividad.",
                "Portatiles", "2378918", 0, R.drawable.pimage2));
        listProduct.add(new Product(100027690, "Lenovo Y530 Corei7-8750H GTX1050Ti 8GB 1TB Win10",
                "Esta laptop de 15.6” te da exactamente lo que necesitas para una experiencia de juego que " +
                        "equilibra rendimiento y portabilidad. Su diseño impresionantemente elegante y sus " +
                        "especificaciones de última generación te garantizan una gran potencia.",
                "Portatiles", "3849000", 0, R.drawable.pimage1));
        listProduct.add(new Product(100129354, "Asus X510qa-Br017t AMD A12 9720p 12GB Win10 15.6",
                "Disfruta de un mundo de color con la tecnología ASUS Splendid. La tecnología de optimización " +
                        "visual ASUS Splendid proporciona las mejores imágenes para cualquier tipo de contenido. " +
                        "Cuenta con cuatro modos de visualización a los que se puede acceder con un solo clic.",
                "Portatiles", "1547000", 4, R.drawable.pimage4));
        listProduct.add(new Product(100338837, "Asus TUF FX504GE-EN756T Core i5 8300H-GTX1050Ti-15,6",
                "El Asus Tuf Gaming FX504GE es un potente portátil línea gamers  con Windows 10 que ofrece " +
                        "una experiencia de juego de inmersión y una durabilidad extrema. El patentado sistema de " +
                        "refrigeración Anti-Dust Cooling (ADC) asegura longevidad y rendimiento estable, mientras que " +
                        "un impresionante panel de 120Hz y sonido envolvente de 7.1 canales activan completamente tus " +
                        "sentidos.",
                "Portatiles", "2799000", 5, R.drawable.pimage3));

        ProductRepository productRepository = new ProductRepository(this);

        for (Product product : listProduct) {
            productRepository.insertProductos(product);
        }

        ProductFirestore productFirestore = new ProductFirestore(this);

        productFirestore.insertProductos(listProduct);

    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_categories:
                    categoriesFragment = new CategoriesFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_layout, categoriesFragment)
                            .commit();

                    return true;
                case R.id.navigation_cart:
                    cartFragment = new CartFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_layout, cartFragment)
                            .commit();

                    return true;
            }
            return false;
        }
    };


}
