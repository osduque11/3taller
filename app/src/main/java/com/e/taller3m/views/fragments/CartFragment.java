package com.e.taller3m.views.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.e.taller3m.R;
import com.e.taller3m.adapters.CartAdapter;
import com.e.taller3m.database.AppDatabase;
import com.e.taller3m.database.CartShoppingRepository;
import com.e.taller3m.database.ProductFirestore;
import com.e.taller3m.database.ProductRepository;
import com.e.taller3m.database.ShoppingCarFirestore;
import com.e.taller3m.database.ShoppingCartFirestore;
import com.e.taller3m.model.Product;
import com.e.taller3m.model.ShoppingCart;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class CartFragment extends Fragment implements View.OnClickListener {
    AppDatabase db;

    ArrayList<ShoppingCart> listProductCart;

    CartShoppingRepository shoppingRepository;
    ProductRepository productRepository;

    ProductFirestore productFirestore;
    ShoppingCarFirestore shoppingCarFirestore;

    RecyclerView rvCartproducts;
    LinearLayoutManager llManager;
    CartAdapter cartAdapter;
    private Context mContext;
    private View mView;
    Button btnClearData;

    public CartFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_cart, container, false);
        mContext = mView.getContext();
        db = Room.databaseBuilder(mView.getContext(), AppDatabase.class, "bestDeal").build();
        listProductCart = new ArrayList<>();
        shoppingRepository = new CartShoppingRepository(mContext);
        productRepository = new ProductRepository(mContext);

        shoppingCarFirestore = new ShoppingCartFirestore(mContext);
        productFirestore = new ProductFirestore(mContext);

        //listProductCart = (ArrayList<ShoppingCart>) shoppingCarFirestore.getAll();
       // Log.i(CartFragment.class.getSimpleName(), "setListProductCart: Carrito =>" + listProductCart.toString());


        initUI();
        getData();

        return mView;
    }



    private void initUI() {
        btnClearData = mView.findViewById(R.id.btnClearData);
        rvCartproducts = mView.findViewById(R.id.rvCartproducts);
        cartAdapter = new CartAdapter(listProductCart);
        llManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        rvCartproducts.setAdapter(cartAdapter);
        rvCartproducts.setLayoutManager(llManager);
        btnClearData.setOnClickListener(this);

        if (listProductCart.isEmpty()) {
            btnClearData.setVisibility(View.GONE);
        }
    }

    private void getData() {
        shoppingCarFirestore.getAll().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                listProductCart.clear();
                for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()){
                    Log.i(CartFragment.class.getSimpleName(), "onSuccess: Data =>"+doc.getData());
                    ShoppingCart shoppingCart = doc.toObject(ShoppingCart.class);
                    listProductCart.add(shoppingCart);
                }
                cartAdapter.setData(listProductCart);
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClearData:

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setTitle(R.string.title_delete_cart);

                builder.setMessage(R.string.delete_cart);

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (final ShoppingCart shoppingCart : listProductCart) {
                            productFirestore.getByCodigo(shoppingCart.getCode()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    Product productUpdate = documentSnapshot.toObject(Product.class);
                                    assert productUpdate != null;
                                    productFirestore.updateStockByProductCode(productUpdate);
                                    shoppingCarFirestore.deleteProduct(shoppingCart);
                                    cartAdapter.updateData();
                                    btnClearData.setVisibility(View.GONE);

                                }
                            });
                        }
                    }
                });

                builder.show();

                break;
        }
    }
}
