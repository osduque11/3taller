package com.e.taller3m.views.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import com.e.taller3m.R;
import com.e.taller3m.database.CartShoppingRepository;
import com.e.taller3m.database.ProductFirestore;
import com.e.taller3m.database.ProductRepository;
import com.e.taller3m.database.ShoppingCartFirestore;
import com.e.taller3m.model.Product;
import com.e.taller3m.model.ShoppingCart;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {
    Product currentProduct;
    ImageView ivImageProduct;
    TextView tvQuantity;
    Button btnIncr;
    Button btnDecr;
    TextView tvDesc;
    TextView tvCod;
    TextView tvPric;

    int cant = 1;

    ProductRepository productRepository;
    ProductFirestore productFirestore;
    CartShoppingRepository shoppingRepository;
    ShoppingCartFirestore shoppingCartFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentProduct = getIntent().getParcelableExtra("currentProduct");
        initUI();

        productRepository = new ProductRepository(this);
        shoppingRepository = new CartShoppingRepository(this);
        productFirestore = new ProductFirestore(this);
        shoppingCartFirestore = new ShoppingCartFirestore(this);

        if (currentProduct != null) {
            getSupportActionBar().setTitle(currentProduct.getName());
            cant = (currentProduct.getStock() == 0) ? 0 : 1;
            tvQuantity.setText(String.valueOf(cant));
            if (currentProduct.getIdImage() != 0) {
                ivImageProduct.setImageDrawable(AppCompatResources.getDrawable(this, currentProduct.getIdImage()));
            }
            tvDesc.setText(currentProduct.getDescription());
            tvCod.setText(String.valueOf(currentProduct.getCode()));
            tvPric.setText(currentProduct.getPrice());
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());

                dialog.setTitle(R.string.save_product);

                dialog.setMessage(R.string.save_item_product);

                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (currentProduct.getStock() > 0) {
                            if (cant > 0) {
                                currentProduct.setStock(currentProduct.getStock() - cant);
                                productFirestore.updateStockByProductCode(currentProduct);
                                ShoppingCart tempShopProduc = new ShoppingCart(currentProduct.getCode(),
                                        currentProduct.getName(), cant, currentProduct.getPrice(), currentProduct.getIdImage());
                                shoppingCartFirestore.insertProductos(tempShopProduc);
                                Toast.makeText(ProductDetailActivity.this, "EL producto ha sido añadido al carrito de compras", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(ProductDetailActivity.this, "Debe seleccionar una cantidad mayor a cero", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(ProductDetailActivity.this, "No hay productos en stock", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
                dialog.show();
            }
        });

        btnDecr.setOnClickListener(this);
        btnIncr.setOnClickListener(this);

    }

    private void initUI() {
        ivImageProduct = findViewById(R.id.ivImage);
        tvDesc = findViewById(R.id.tvDescription);
        tvCod = findViewById(R.id.tvCode);
        tvPric = findViewById(R.id.tvPrice);
        btnIncr = findViewById(R.id.btnIncrem);
        btnDecr = findViewById(R.id.btnDecre);
        tvQuantity = findViewById(R.id.tvQueantitySel);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);


        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDecre:
                if (cant != 0) {
                    cant--;
                    tvQuantity.setText(String.valueOf(cant));
                    if (cant == 0) {
                        btnDecr.setEnabled(false);
                    }
                }

                break;

            case R.id.btnIncrem:
                if (cant < currentProduct.getStock()) {
                    cant++;
                    tvQuantity.setText(String.valueOf(cant));
                    if (cant > 0) {
                        btnDecr.setEnabled(true);
                    }
                }

                break;
        }
    }
}
