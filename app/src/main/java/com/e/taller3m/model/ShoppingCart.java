package com.e.taller3m.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity
public class ShoppingCart {

    @PrimaryKey(autoGenerate = true)
    private  int id;

    @ColumnInfo(name = "code")
    private int code;


    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "quantity")
    private int quantity;

    @ColumnInfo(name = "price")
    private String price;

    @ColumnInfo(name = "idImage")
    private int idImage;

    public ShoppingCart() {
    }
    @Ignore
    public ShoppingCart(int code, String name, int quantity, String price, int idImage) {
        this.code = code;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.idImage = idImage;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "id=" + id +
                ", code=" + code +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", price='" + price + '\'' +
                ", idImage=" + idImage +
                '}';
    }
}
